<?php

namespace IB\FormulaInterpreter\Exception;

/**
 * Description of FunctionParser.
 *
 * @author mathieu
 */
class NotEnoughArgumentsException extends \Exception
{
}
