<?php

namespace IB\FormulaInterpreter;

/**
 * Description of Compiler.
 *
 * @author mathieu
 */
class Executable
{
    /**
     * @var Command\CommandInterface
     */
    protected $command;

    /**
     * @var \ArrayObject
     */
    protected $variables;

    public function __construct(Command\CommandInterface $command, \ArrayObject $variables)
    {
        $this->command = $command;
        $this->variables = $variables;
    }

    public function getParameters(): array
    {
        return $this->command->getParameters();
    }

    public function run($variables = [])
    {
        $this->variables->exchangeArray($variables);

        return $this->command->run();
    }
}
