<?php

namespace IB\FormulaInterpreter\Parser;

/**
 * Description of FunctionParser.
 *
 * @author mathieu
 */
class CompositeParser implements ParserInterface
{
    protected $parsers = [];

    public function addParser(ParserInterface $parser)
    {
        $this->parsers[] = $parser;
    }

    public function parse($expression)
    {
        foreach ($this->parsers as $parser) {
            try {
                return $parser->parse($expression);
            } catch (ParserException $e) {
                if ($e->getExpression() != trim($expression)) {
                    throw $e;
                }
            }
        }

        throw new ParserException($expression);
    }
}
