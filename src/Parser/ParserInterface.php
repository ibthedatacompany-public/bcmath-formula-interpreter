<?php

namespace IB\FormulaInterpreter\Parser;

/**
 * Description of FunctionParser.
 *
 * @author mathieu
 */
interface ParserInterface
{
    public function parse($exception);
}
