<?php

namespace IB\FormulaInterpreter\Command;

/**
 * Description of FunctionParser.
 *
 * @author mathieu
 */
class NumericCommand implements CommandInterface
{
    /**
     * @var string
     */
    protected $value;

    public function __construct($value)
    {
        if (!is_numeric($value)) {
            $message = sprintf(
                'Parameter $value of method __construct() of class %s must be a numeric string. Got %s type instead.',
                get_class($this),
                gettype($value)
            );
            throw new \InvalidArgumentException($message);
        }

        $this->value = $value;
    }

    public function run()
    {
        return $this->value;
    }

    public function getParameters(): array
    {
        return [];
    }

    public static function create($options)
    {
    }
}
