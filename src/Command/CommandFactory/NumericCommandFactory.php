<?php

namespace IB\FormulaInterpreter\Command\CommandFactory;

use IB\FormulaInterpreter\Command\NumericCommand;

/**
 * Description of FunctionParser.
 *
 * @author mathieu
 */
class NumericCommandFactory implements CommandFactoryInterface
{
    /**
     * @return \IB\FormulaInterpreter\Command\CommandInterface
     */
    public function create($options)
    {
        if (!isset($options['value'])) {
            throw new CommandFactoryException();
        }

        return new NumericCommand($options['value']);
    }
}
