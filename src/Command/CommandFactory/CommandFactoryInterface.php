<?php

namespace IB\FormulaInterpreter\Command\CommandFactory;

/**
 * Description of FunctionParser.
 *
 * @author mathieu
 */
interface CommandFactoryInterface
{
    /**
     * @param array $options
     *
     * @return \IB\FormulaInterpreter\Command\CommandInterface
     */
    public function create($options);
}
