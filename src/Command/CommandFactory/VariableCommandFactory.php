<?php

namespace IB\FormulaInterpreter\Command\CommandFactory;

use IB\FormulaInterpreter\Command\VariableCommand;

/**
 * Description of FunctionParser.
 *
 * @author mathieu
 */
class VariableCommandFactory implements CommandFactoryInterface
{
    protected $variables;

    public function __construct($variables)
    {
        $this->variables = $variables;
    }

    /**
     * @return \IB\FormulaInterpreter\Command\CommandInterface
     */
    public function create($options)
    {
        if (!isset($options['name'])) {
            throw new CommandFactoryException();
        }

        return new VariableCommand(
            $options['name'],
            $this->variables
        );
    }
}
