<?php

namespace IB\FormulaInterpreter\Command\CommandFactory;

use IB\FormulaInterpreter\Command\FunctionCommand;

/**
 * Description of FunctionParser.
 *
 * @author mathieu
 */
class FunctionCommandFactory implements CommandFactoryInterface
{
    protected $functions = [];

    /**
     * @var CommandFactoryInterface
     */
    protected $argumentCommandFactory;

    public function __construct(CommandFactoryInterface $argumentCommandFactory)
    {
        $this->argumentCommandFactory = $argumentCommandFactory;
    }

    public function registerFunction($name, $callable)
    {
        $this->functions[$name] = $callable;
    }

    /**
     * @return \IB\FormulaInterpreter\Command\CommandInterface
     */
    public function create($options)
    {
        if (!isset($options['name'])) {
            throw new CommandFactoryException('Missing option "name"');
        }

        if (!isset($this->functions[$options['name']])) {
            throw new \IB\FormulaInterpreter\Exception\UnknownFunctionException($options['name']);
        }

        $argumentCommands = [];

        if (isset($options['arguments'])) {
            foreach ($options['arguments'] as $argumentOptions) {
                $argumentCommands[] = $this->argumentCommandFactory->create($argumentOptions);
            }
        }

        return new FunctionCommand($this->functions[$options['name']], $argumentCommands);
    }
}
