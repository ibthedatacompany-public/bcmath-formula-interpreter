<?php

namespace IB\FormulaInterpreter\Command;

/**
 * Description of FunctionParser.
 *
 * @author mathieu
 */
interface CommandInterface
{
    public function run();

    public function getParameters(): array;
}
