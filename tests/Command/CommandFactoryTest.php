<?php

use IB\FormulaInterpreter\Command\CommandFactory;

/**
 * Description of ParserTest.
 *
 * @author mathieu
 */
class CommandFactoryTest extends \PHPUnit\Framework\TestCase
{
    public function testCreate()
    {
        $factory = new CommandFactory();

        $command = new CommandFactoryTest_FakeCommand();
        $numericFactory = $this
            ->getMockBuilder('\IB\FormulaInterpreter\Command\CommandFactory')
            ->getMock();

        $numericFactory->expects($this->once())
                ->method('create')
                ->will($this->returnValue($command));
        $factory->registerFactory('numeric', $numericFactory);

        $this->assertEquals($factory->create(['type' => 'numeric']), $command);
    }

    /**
     * @expectedException \IB\FormulaInterpreter\Command\CommandFactory\CommandFactoryException
     */
    public function testMissingTypeOption()
    {
        $factory = new CommandFactory();

        $factory->create([]);
    }

    /**
     * @expectedException \IB\FormulaInterpreter\Command\CommandFactory\CommandFactoryException
     */
    public function testUnknownType()
    {
        $factory = new CommandFactory();

        $factory->create(['type' => 'numeric']);
    }
}

class CommandFactoryTest_FakeCommand implements \IB\FormulaInterpreter\Command\CommandInterface
{
    public function run()
    {
    }

    public function getParameters(): array
    {
        return [];
    }
}
