<?php

use IB\FormulaInterpreter\Command\CommandFactory\NumericCommandFactory;
use IB\FormulaInterpreter\Command\NumericCommand;

/**
 * Description of NumericCommandFactory.
 *
 * @author mathieu
 */
class NumericCommandFactoryTest extends \PHPUnit\Framework\TestCase
{
    /**
     *  @dataProvider getNumbers
     */
    public function testCreate($value)
    {
        $factory = new NumericCommandFactory();
        $options = ['value' => $value];
        $this->assertEquals($factory->create($options), new NumericCommand($value));
    }

    public function getNumbers()
    {
        return [
            ['2'],
            ['4'],
        ];
    }

    /**
     * @expectedException \IB\FormulaInterpreter\Command\CommandFactory\CommandFactoryException
     */
    public function testCreateWithMissingValueOption()
    {
        $factory = new NumericCommandFactory();
        $factory->create([]);
    }
}
