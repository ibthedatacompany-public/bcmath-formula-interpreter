<?php

use IB\FormulaInterpreter\Command\CommandFactory\FunctionCommandFactory;
use IB\FormulaInterpreter\Command\FunctionCommand;

/**
 * Description of NumericCommandFactory.
 *
 * @author mathieu
 */
class FunctionCommandFactoryTest extends \PHPUnit\Framework\TestCase
{
    public function setUp()
    {
        $this->argumentCommandFactory = $this->getMockBuilder(
            '\IB\FormulaInterpreter\Command\CommandFactory\CommandFactoryInterface'
        )->getMock();
        $this->factory = new FunctionCommandFactory($this->argumentCommandFactory);
        $this->piFunction = function () { return 3.14; };
        $this->factory->registerFunction('pi', $this->piFunction);
    }

    public function testCreateShouldReturnFunctionCommand()
    {
        $options = ['name' => 'pi'];
        $object = $this->factory->create($options);
        $this->assertTrue($object instanceof FunctionCommand, 'An instance of FunctionCommand should be returned');
    }

    public function testCreateWithNoArguments()
    {
        $options = ['name' => 'pi'];
        $object = $this->factory->create($options);
        $this->assertObjectPropertyEquals($object, 'callable', $this->piFunction);
        $this->assertObjectPropertyEquals($object, 'argumentCommands', []);
    }

    public function testCreateWithArguments()
    {
        $argumentCommand = $this->getMockBuilder(
            'IB\FormulaInterpreter\Command\CommandInterface'
        )->getMock();
        $this->argumentCommandFactory->expects($this->once())
                ->method('create')
                ->with($this->equalTo(['type' => 'fake']))
                ->will($this->returnValue($argumentCommand));

        $options = [
            'name' => 'pi',
            'arguments' => [['type' => 'fake']],
        ];
        $object = $this->factory->create($options);
        $this->assertObjectPropertyEquals($object, 'callable', $this->piFunction);
        $this->assertObjectPropertyEquals($object, 'argumentCommands', [$argumentCommand]);
    }

    /**
     * @expectedException \IB\FormulaInterpreter\Exception\UnknownFunctionException
     */
    public function testCreateWithNotExistingFunction()
    {
        $options = [
            'name' => 'notExistingFunction',
        ];
        $this->factory->create($options);
    }

    /**
     * @expectedException \IB\FormulaInterpreter\Command\CommandFactory\CommandFactoryException
     */
    public function testCreateWithMissingNameOption()
    {
        $this->factory->create([]);
    }

    protected function assertObjectPropertyEquals($object, $property, $expected)
    {
        $this->assertEquals(\PHPUnit\Framework\Assert::readAttribute($object, $property), $expected);
    }
}
