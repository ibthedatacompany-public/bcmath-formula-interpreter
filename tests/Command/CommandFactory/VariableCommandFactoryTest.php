<?php

use IB\FormulaInterpreter\Command\CommandFactory\VariableCommandFactory;
use IB\FormulaInterpreter\Command\VariableCommand;

/**
 * Description of VariableCommandFactory.
 *
 * @author mathieu
 */
class VariableCommandFactoryTest extends \PHPUnit\Framework\TestCase
{
    /**
     *  @dataProvider getNumbers
     */
    public function testCreate($name, $variables)
    {
        $factory = new VariableCommandFactory($variables);
        $options = ['name' => $name];
        $this->assertEquals($factory->create($options), new VariableCommand($name, $variables));
    }

    public function getNumbers()
    {
        return [
            ['rate', ['rate' => 4]],
            ['price', ['price' => 4]],
            ['price', ['price' => 40]],
        ];
    }

    /**
     * @expectedException \IB\FormulaInterpreter\Command\CommandFactory\CommandFactoryException
     */
    public function testCreateWithMissingNameOption()
    {
        $factory = new VariableCommandFactory([]);
        $factory->create([]);
    }
}
