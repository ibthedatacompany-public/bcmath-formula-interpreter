<?php

use IB\FormulaInterpreter\Command\NumericCommand;

/**
 * Description of ParserTest.
 *
 * @author mathieu
 */
class NumericCommandTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @dataProvider getNumbers
     */
    public function testRun($value, $result)
    {
        $command = new NumericCommand($value);

        $this->assertEquals($command->run(), $result);
    }

    public function getNumbers()
    {
        return [
            [2, 2],
            [2.2, 2.2],
        ];
    }

    /**
     * @expectedException \InvalidArgumentException
     * @dataProvider getIncorrectValues
     */
    public function testInjectIncorrectValue($value)
    {
        $command = new NumericCommand($value);
        $command->run();
    }

    public function getIncorrectValues()
    {
        return [
            ['string'],
            [false],
            [[]],
        ];
    }
}
