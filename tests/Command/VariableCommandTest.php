<?php

use IB\FormulaInterpreter\Command\VariableCommand;

/**
 * Description of ParserTest.
 *
 * @author mathieu
 */
class VariableCommandTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @dataProvider getNumbers
     */
    public function testRunWhenVariablesExists($name, $variables, $result)
    {
        $command = new VariableCommand($name, $variables);

        $this->assertEquals($command->run(), $result);
    }

    public function getNumbers()
    {
        return [
            ['rate', ['rate' => 2], 2],
            ['price', ['price' => 32.2], 32.2],
        ];
    }

    /**
     * @expectedException \IB\FormulaInterpreter\Exception\UnknownVariableException
     */
    public function testRunWhenVariableNotExists()
    {
        $command = new VariableCommand('rate', []);
        $command->run();
    }

    public function testRunWhenVariablesHolderImplementsArrayAccess()
    {
        $variables = $this->getMockBuilder('\ArrayAccess')->getMock();
        $variables->expects($this->any())
            ->method('offsetExists')
            ->with($this->equalTo('rate'))
            ->will($this->returnValue(true));
        $variables->expects($this->any())
            ->method('offsetGet')
            ->with($this->equalTo('rate'))
            ->will($this->returnValue(23));

        $command = new VariableCommand('rate', $variables);

        $this->assertEquals($command->run(), 23);
    }

    /**
     * @expectedException \InvalidArgumentException
     * @dataProvider getIncorrectNames
     */
    public function testInjectIncorrectName($name)
    {
        $command = new VariableCommand($name, []);
    }

    public function getIncorrectNames()
    {
        return [
            [12],
            [false],
            [[]],
            [new StdClass()],
        ];
    }

    /**
     * @expectedException \InvalidArgumentException
     * @dataProvider getIncorrectVariables
     */
    public function testInjectIncorrectVariables($variables)
    {
        $command = new VariableCommand('rate', $variables);
    }

    public function getIncorrectVariables()
    {
        return [
            [12],
            [false],
            ['string'],
            [new StdClass()],
        ];
    }
}
